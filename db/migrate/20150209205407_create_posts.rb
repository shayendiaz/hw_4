class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :author
      t.datetime :created_at
      t.boolean :private

      t.timestamps null: false
    end
  end
end
